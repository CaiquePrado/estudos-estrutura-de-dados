# Estudos Estrutura De Dados

### O que são estrutura de dados?
- Os programas de computador, na sua grande maioria (para não dizer na sua quase totalidade), trabalham sobre conjuntos de dados.

![alt text](image.png) 

Exemplo algoritmo de ordenação.

- Os dados a serem processados têm que estar na memória, assim como os dados
processados também são depositados em memória.

- De forma que é necessário um meio de transportar esses dados do mundo externo
para a memória e também um meio de apresentar ao mundo externo os dados
processados.

![alt text](image-1.png)

1. Os dados transportados para a memória têm que ficar dispostos de forma que o
algoritmo consiga acessá-los com eficiência.

2. Caso se trate de um dado único, uma variável resolve o problema, mas se há um
conjunto de dados, se faz necessário um conjunto de variáveis estruturadas de
forma prática. 

3. Este armazenamento na memória é o que caracteriza as Estruturas de Dados deste
nosso estudo.

4. Para cada tipo de problema existe uma estrutura mais adequada. Uma boa estrutura
faz muita diferença no desempenho do programa. 